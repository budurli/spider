# -*- coding: utf-8 -*-

import re

import gevent.monkey

gevent.monkey.patch_socket()

import requests

href_pattern = re.compile(r'href="(.*?)"')


class BaseSpider(object):
    name = 'BaseSpider'
    site_tree = set()
    allowed_domains = set()

    def __init__(self, base_url):
        self.base_url = base_url
        self.url_alive(self.base_url)
        self.allowed_domains.add(base_url)
        self.site_tree.add(base_url)

    def url_alive(self, url):
        success = requests.get(url)
        return success.status_code in [200, 301, 302]

    def get_page_tree(self, url):
        result = set()

        if self.base_url not in url:
            this_url = self.base_url + url
        else:
            this_url = url

        try:
            page = requests.get(this_url)

            for href in href_pattern.findall(page.text):

                if href.startswith('/'):
                    href = self.base_url + href

                for domain in list(self.allowed_domains):
                    if domain in href:
                        result.add(href)

        except Exception as err:
            print u'Error %s with %s' % (str(err), this_url)

        return list(result)