# -*- coding: utf-8 -*-

import os

from crawler import Crawler


here = lambda *x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

PROJECT_ROOT = here('..')

root = lambda *x: os.path.join(os.path.abspath(PROJECT_ROOT), *x)


class MirrorSpider(Crawler):
    """
    "Зеркало" сайта. Копирует всю доступную структуру в указанную папку
    """

    def __init__(self, base_url, directory_name=None):
        super(MirrorSpider, self).__init__(base_url)
        self.directory_name = directory_name or (PROJECT_ROOT + 'mirror')

    def create_tree(self, path):
        os.makedirs(self.directory_name + path)

    def handle_response(self, response):
        self.crawled.add(response.url)
        if u'text/html' in response.headers.get('content-type'):
            print response.url
