# -*- coding: utf-8 -*-
import re

from gevent.pool import Pool
import requests

from spider.base import BaseSpider


class Crawler(BaseSpider):
    name = 'Crawler'
    already_was_in = set()
    crawled = set()
    max_requests = 30

    def __init__(self, base_url):
        super(Crawler, self).__init__(base_url)

    def start(self):
        self.crawl(self.base_url)

    def handle_response(self, response):
        self.crawled.add(response.url)
        print u'%s  - %s ' % (self.name, response.url)

    def check_urls(self, urls):
        def fetch(url):
            response = requests.request('GET', url, timeout=5.0)
            if 200 == response.status_code and response.url not in self.crawled:
                self.handle_response(response)

        pool = Pool(self.max_requests)

        for url in urls:
            pool.spawn(fetch, url)
        pool.join()

    def crawl(self, list_of_urls):

        if isinstance(list_of_urls, str):
            list_of_urls = [list_of_urls]

        for url in list_of_urls:
            self.crawled.add(url)
            new_urls = set(self.get_page_tree(url)) - self.crawled
            self.check_urls(new_urls)
            self.crawl(new_urls)

    def add_list_to_sitemap(self, list_of_urls):
        self.site_tree |= set(list_of_urls)


class Parser(Crawler):
    name = 'Parser'

    def __init__(self, base_url, urls_in_regulars):
        super(Parser, self).__init__(base_url)
        self.urls_to_parse = "(" + ")|(".join(urls_in_regulars) + ")"

    def handle_response(self, response):
        self.crawled.add(response.url)
        if re.match(self.urls_to_parse, response.url):
            self.parse(response)

    def parse(self, response):
        print u'%s  - %s ' % (self.name, response.url)
